﻿var rng = new Random();

Monster troll = new("Troll", maxHp: 100);
Monster orc = new("Orc", maxHp: 100);

Console.WriteLine(troll);
Console.WriteLine(orc);

var turns = 0;

while (true)
{
    ++turns;
    Console.WriteLine();
    Console.WriteLine($"Turn {turns}");
    Console.WriteLine($"============");

    bool fainted = DoTurn(rng, troll, orc);
    Console.WriteLine(troll);
    Console.WriteLine(orc);
    if (fainted)
    {
        break;
    }
}

Console.WriteLine();
if (!troll.Fainted)
{
    Console.WriteLine("Troll wins!");
}
else
{
    Console.WriteLine("Orc wins!");
}

bool DoTurn(Random rng, Monster monster1, Monster monster2)
{
    var oneGoesFirst = rng.Next(2) == 0;
    Monster first;
    Monster second;
    if (oneGoesFirst)
    {
        first = monster1;
        second = monster2;
    }
    else
    {
        first = monster2;
        second = monster1;
    }

    first.Attack(rng, second);
    if (second.Fainted)
    {
        return true;
    }

    second.Attack(rng, first);
    if (first.Fainted)
    {
        return true;
    }

    return false;
}

class Monster
{
    private int hp;

    public Monster(string name, int maxHp)
    {
        Name = name;
        MaxHp = maxHp;
        hp = maxHp;
    }

    public void Attack(Random rng, Monster opponent)
    {
        if (opponent is null)
        {
            throw new ArgumentNullException(nameof(opponent));
        }

        if (object.ReferenceEquals(this, opponent))
        {
            throw new ArgumentException("Monster cannot attack itself.", nameof(opponent));
        }

        Console.WriteLine($"{Name} attacks!");

        int damage = 5 + rng.Next(3);
        bool crit = rng.Next(20) == 0;
        if (crit)
        {
            damage *= 2;
        }
        if (crit)
        {
            Console.WriteLine($"{Name} hit a critical strike !!!");
        }
        Console.WriteLine($"{Name} did {damage} damage to their opponent!");
        opponent.Hp -= damage;
    }

    public override string ToString()
    {
        return $"{Name}: {Hp}/{MaxHp}";
    }

    public string Name { get; init; }

    public int MaxHp { get; init; }

    public int Hp
    {
        get => hp;
        set
        {
            hp = value;
            if (value > MaxHp)
            {
                hp = MaxHp;
            }
            if (value <= 0)
            {
                Console.WriteLine($"{Name} fainted!");
                hp = 0;
            }
        }
    }

    public bool Fainted { get => Hp < 1; }
}
